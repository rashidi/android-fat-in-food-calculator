package my.zin.rashidi.android.fatcal;

import my.zin.rashidi.android.fatcal.dialog.DialogHelper;
import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * 
 * @author shidi
 * @version 1.1
 * @since 1.0.0
 */
public class FatInFoodCalculatorActivity extends Activity {
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	setContentView(R.layout.main);
    	
    	final Button buttonCalc = (Button) findViewById(R.id.buttonCalculate);
    	buttonCalc.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				final EditText txtTotalCalories = (EditText) findViewById(R.id.editTotalCalorie);
				final EditText txtTotalFat = (EditText) findViewById(R.id.editTotalFat);

				Double totalFatPercentage = calculateFat(txtTotalCalories.getText().toString(), txtTotalFat.getText().toString());
				
				if (totalFatPercentage != null) {
					validateFatValue(txtTotalFat);
					final EditText txtTotalFatPercentage = (EditText) findViewById(R.id.editTotalFatPercentage);
					txtTotalFatPercentage.setText(String.format("%.2f%s", totalFatPercentage, '%'));
					txtTotalFatPercentage.setEnabled(false);
				}
			}
		});
    	
    	final Button buttonClear = (Button) findViewById(R.id.buttonClear);
    	buttonClear.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((EditText) findViewById(R.id.editTotalCalorie)).setText("");
				((EditText) findViewById(R.id.editTotalFat)).setText("");
				((EditText) findViewById(R.id.editTotalFatPercentage)).setText("");
			}
		});
    	
    }
    
    private Double calculateFat(String calories, String fat) {
    	if (calories.length() == 0) {
    		AlertDialog cancel = DialogHelper.cancelDialog(this, "Total Calorie is required");
    		cancel.show();
    	} else {
			Double totalCalories = Double.valueOf(calories);
			Double totalFat = (fat.length() == 0) ? 0 : Double.valueOf(fat);
			
			return (((totalFat * 9) / totalCalories) * 100);
    	}
    	
    	return null;
    }
    
    private void validateFatValue(EditText txtTotalFat) {
    	if (txtTotalFat.getText().toString().length() == 0) {
    		txtTotalFat.setText(String.valueOf(0));
    	}
    }

}