/**
 * 
 */
package my.zin.rashidi.android.fatcal.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;


/**
 * @author shidi
 * @version 1.0.1
 * @since 1.0.1
 */
public class DialogHelper {
	
	public DialogHelper() {
		
	}
	
	public static AlertDialog cancelDialog(Context context, String msg) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(msg).setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		
		return builder.create();
	}

}
